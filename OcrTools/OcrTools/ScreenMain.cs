﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OcrTools
{
    public partial class ScreenMain : Form
    { 

        int x, y, nowX, nowY, width, height;
        bool isMouthDown = false;
        bool Ocring = false;
        Graphics g;

        public ScreenMain()
        {
            InitializeComponent();
        }

        private void Form2_MouseDown(object sender, MouseEventArgs e)
        {
            x = MousePosition.X;
            y = MousePosition.Y;
            isMouthDown = true;
        }

        private void Form2_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouthDown && !Ocring)
            {
                width = Math.Abs(MousePosition.X - x);
                height = Math.Abs(MousePosition.Y - y);
                g = CreateGraphics();
                g.Clear(BackColor);
                g.FillRectangle(Brushes.CornflowerBlue, x < MousePosition.X ? x : MousePosition.X, y < MousePosition.Y ? y : MousePosition.Y, width + 1, height + 1);
            }
        }

        private void Form2_MouseUp(object sender, MouseEventArgs e)
        {
            nowX = MousePosition.X + 1;
            nowY = MousePosition.Y + 1;
            this.Close();
            //formMain.pcurrentWin.sendEndMes += tellEnd;
            // 开启线程处理数据
            Task t = new(() =>
            {
                Ocring = true;
                formMain.pcurrentWin.Snap(x < nowX ? x : nowX, y < nowY ? y : nowY, Math.Abs(nowX - x), Math.Abs(nowY - y));
            }); 
            t.Start();
            formMain.pcurrentWin.Show();
        }

        /// <summary>
        /// 通知load数据结束的方法
        /// 此方法仍为子线程中的方法，因为被子线程中的委托调用
        /// </summary>
        /// <param name="mes"></param>
        private void tellEnd(string mes)
        {
            Ocring = false;
            // 处理数据完成后的提示信息
            formMain.pcurrentWin.Show(); 
        } 
    }
}
